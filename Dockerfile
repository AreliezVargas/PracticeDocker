FROM frolvlad/alpine-oraclejdk8:slim
ENV APP_PATH=/test
RUN mkdir ${APP_PATH}
COPY Main.jar ${APP_PATH}/app.jar
WORKDIR ${APP_PATH}
#ENTRYPOINT ["java", "-jar", "app.jar"]
CMD ["java", "-jar", "app.jar"]